
const fs = require('fs');
const http = require('http');
const https = require('https');
const mmm = require('mmmagic');
const Magic = mmm.Magic;

function Downloader() {
	this.magic = new Magic(mmm.MAGIC_MIME_TYPE);
	this._progress_smart = [];
}

Downloader.prototype.smart_download = function download(url, destination) {
	// if destination exists return
	try {
		let stats = fs.lstatSync(destination);
		console.log(stats, stats.isFile(), stats.isDirectory(), stats.isSymbolicLink());
		if (stats.isFile()) {
			console.log('  '+destination+' exists already, not downloading.');
			return destination;
		} else if (stats.isSymbolicLink()) {
			console.log(' is symlink!',destination);
			let target = fs.readlinkSync(destination);
			if (fs.existsSync(target)) {
				console.log('  '+destination+' exists already and points to '+target+', not downloading.');
				return target;
			} else {
				console.log(' uh, something is off...', destination);
				// target missing, clean up symlink
				fs.unlinkSync(destination);
			}
		} else {
			console.log(' something is wrong!', destination);
		}
	} catch (e) {
		console.warn('  failed assessing '+destination+' (probably it just doesn\'t exists)', e);
	}
	// download url
	console.log('  downloading '+destination+'...');
	this._add_dl(url, destination);
	return destination;
};

Downloader.prototype.replace_renamed_files = async function replace_renamed_files(out) {
	for (let i in this._progress_smart) {
		out = out.replace(
			'{'+this._progress_smart[i][0]+'}',
			'{'+(await this._progress_smart[i][1])+'}'
		);
		console.log('replaced '+this._progress_smart[i][0]+' with '+(await this._progress_smart[i][1]));
	}
	return out;
};

Downloader.prototype.wait_until_finished = function wait_until_finished() {
	return Promise.allSettled(this._progress_smart.map(e => e[2]));
};

Downloader.prototype._add_dl = function add_dl(url, destination) {
	let promise = new Promise((res, rej) => {
		mkdir_p_sync(destination.replace(/\/.*$/, ''));
		const file = fs.createWriteStream(destination);
		const request = (url.startsWith('https://') ? https : http).get(url, response => {
			response.pipe(file);
			file.on('finish', () => {
				// if file extension does not match mime
				this.magic.detectFile(destination, (err, result) => {
					if (err) throw err;

					let mime = result.split('/');
					if (mime[0] != 'image') {
						//rej(new Error(destination+' is not an image. We don\'t care about non-images.'));
						res(destination);
					}

					let old_ext = destination
						.split('/').pop()	// make sure we only use the filename part
						.split('.').pop();
					if (old_ext && old_ext.length <= 6 && old_ext == mime[1]) {
						console.log('probably correct extension already');
					} else {
						let target = destination+'.'+mime[1];
						// rename with extension
						fs.rename(destination, target, () => {
							ntarget = target.split('/').pop();	// now relative to its position, not to our working dir
							// symlink
							fs.symlink(ntarget, destination, res.bind(this, target));
							console.log(' saved as '+target);
						});
					}
				});
			});
		});
	});

	this._progress_smart.push([destination, promise]);
};


function mkdir_p_sync(path) {
	try {
		if (!fs.statSync(path).isDirectory()) {
			// fail
		} else {
			console.log(path+' already exists');
		}
	} catch (e) {
		if (path.indexOf('/') > -1) {
			mkdir_p_sync(path.replace(/\/.+/, ''));
		}
		console.log(path+' creating...');
		fs.mkdirSync(path);
	}
}


module.exports = Downloader;
